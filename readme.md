# Code challenge for 888spectate

## Requirements

``Python >= 3.6``

## Installation

- First of all download the repository.

- In your CMD or Bash, go to the root directory and run the following command to install all libraries needed:

``pip install -r requirements.txt``

- Create the migrations with the following command:

``python manage.py makemigrations``

- Then run the migrations to create the tables in the database:

``python manage.py migrate``

## Usage

- To run the server locally simply copy and paste the following command in the root directory of the project:

``python manage.py runserver``

- To see the API documentation simply go to:

``localhost:8000/swagger/``