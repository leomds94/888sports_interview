from django.db import models
from rest_framework.reverse import reverse


class Sport(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Market(models.Model):
    name = models.CharField(max_length=50)
    sport = models.ForeignKey(Sport, null=True, on_delete=models.CASCADE, related_name='markets')

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'sport')


class Match(models.Model):
    name = models.CharField(max_length=500)
    startTime = models.DateTimeField()
    sport = models.ForeignKey(Sport, null=True, on_delete=models.CASCADE, related_name='matches')
    markets = models.ManyToManyField(Market)

    # market = models.ForeignKey(Market, null=True, on_delete=models.CASCADE, related_name='matches')

    def __str__(self):
        return self.name

    def get_api_url(self, request=None):
        return reverse('api:match-detail', kwargs={'id': self.id}, request=request)

    class Meta:
        verbose_name_plural = 'Matches'
        unique_together = ('name', 'startTime', 'sport')


class Selection(models.Model):
    name = models.CharField(max_length=150)
    odds = models.FloatField(default=1.0)
    market = models.ForeignKey(Market, null=True, on_delete=models.CASCADE, related_name='selections')

    def __str__(self):
        return str(self.name)


class Message(models.Model):
    message_type = models.CharField(max_length=50)
    event = models.ForeignKey(Match, null=True, on_delete=models.SET_NULL)

    def get_api_url(self, request=None):
        return reverse('api:message', request=request)
