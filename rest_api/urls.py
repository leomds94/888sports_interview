from django.urls import path

from .views import MatchListView, MatchDetailAPIView, MessageAPIView

urlpatterns = [
    path('match/<int:id>', MatchDetailAPIView.as_view(), name='match-detail'),
    path('match/', MatchListView.as_view(), name='match'),
    path('message/', MessageAPIView.as_view(), name='message')
]
