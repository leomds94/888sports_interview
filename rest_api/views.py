from django.http import HttpResponse
from django_filters import rest_framework as filters_rest
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, generics
from rest_framework.filters import OrderingFilter
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_api.models import Match, Message, Selection
from rest_api.serializers import MatchSerializer, MessageSerializer


class MatchFilter(filters_rest.FilterSet):
    name = filters_rest.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Match
        fields = ('name',)


class MatchListView(generics.ListAPIView):
    serializer_class = MatchSerializer
    filterset_class = MatchFilter
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filter_fields = ('name',),

    ordering_fields = ['name', 'startTime']

    def get_queryset(self):
        matches = Match.objects.all()
        name = self.request.query_params.get('name', None)
        ordering = self.request.query_params.get('ordering', None)
        sport = self.request.query_params.get('sport', None)
        if name is not None or ordering is not None or sport is not None:
            if sport is not None:
                sport = sport.title()
                matches = matches.filter(sport__name__icontains=sport)
            matches = matches.only('id', 'name', 'startTime')
        return matches


class MatchDetailAPIView(APIView):
    def get_object(self, id):
        try:
            return Match.objects.get(id=id)
        except Match.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get_serializer_context(self, *args, **kwargs):
        return {'request': self.request}

    def get(self, request, id):
        match = self.get_object(id)
        serializer = MatchSerializer(match, context={'request': Request(request._request)})
        return Response(serializer.data)


class MessageAPIView(APIView):
    def get_serializer_context(self, *args, **kwargs):
        return {'request': self.request}

    def post(self, request):
        if request.data['message_type'] == 'NewEvent':
            serializer = MessageSerializer(data=request.data, context={'request': Request(request._request)})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif request.data['message_type'] == 'UpdateOdds':
            try:
                response = Message.objects.create(message_type=request.data['message_type'])
                request.data['id'] = response.id
                Match.objects.get(id=request.data['event']['id'])
                market = request.data['event']['markets'][0]
                for selection in market['selections']:
                    sel = Selection.objects.get(id=selection['id'])
                    sel.odds = selection['odds']
                    sel.save()
                return Response(request.data, status=status.HTTP_201_CREATED)
            except:
                return Response(status.HTTP_417_EXPECTATION_FAILED)
        return Response(status=status.HTTP_404_NOT_FOUND)
