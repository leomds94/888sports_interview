from django.db.models.functions import datetime
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from .models import Selection, Market, Match, Sport


class SelectionAPITestCase(APITestCase):
    def setUp(self):
        sport = Sport.objects.create(name='sport')
        market = Market.objects.create(name='Winner', sport=sport)
        Selection.objects.create(name='Team1', market=market)

    def test_selection_creation(self):
        selection_count = Selection.objects.count()
        self.assertEqual(selection_count, 1)


class SportAPITestCase(APITestCase):
    def setUp(self):
        Sport.objects.create(name='sport')

    def test_sport_creation(self):
        sport_count = Sport.objects.count()
        self.assertEqual(sport_count, 1)


class MarketAPITestCase(APITestCase):
    def setUp(self):
        sport = Sport.objects.create(name='sport')
        Market.objects.create(name='Winner', sport=sport)

    def test_market_creation(self):
        market_count = Market.objects.count()
        self.assertEqual(market_count, 1)


class MatchAPITestCase(APITestCase):
    datetime_now = datetime.datetime.now()

    def setUp(self):
        sport = Sport.objects.create(name='sport')
        market = Market.objects.create(name='Winner', sport=sport)
        Match.objects.create(name='Team1 vs Team2', startTime=self.datetime_now, market=market, sport=sport)

    def test_match_creation(self):
        match_count = Match.objects.count()
        self.assertEqual(match_count, 1)


class MessageAPITestCase(APITestCase):
    url = reverse('api:message')
    datetime_now = datetime.datetime.now()

    def test_new_event(self):
        new_event = {
            "id": 1,
            "message_type": "NewEvent",
            "event": {
                "id": 1,
                "name": "Real Madrid vs Barcelona",
                "startTime": "2018-06-20 10:30:00",
                "sport": {
                    "id": 1,
                    "name": "Football"
                },
                "markets": [
                    {
                        "id": 1,
                        "name": "Winner",
                        "selections": [
                            {
                                "id": 1,
                                "name": "Real Madrid",
                                "odds": 1.01
                            },
                            {
                                "id": 2,
                                "name": "Barcelona",
                                "odds": 1.01
                            }
                        ]
                    }
                ]
            }
        }
        response = self.client.post(self.url, new_event, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Match.objects.count(), 1)
        self.assertEqual(Match.objects.get().name, 'Real Madrid vs Barcelona')

    def test_update_odds(self):
        new_event = {
            "id": 1,
            "message_type": "NewEvent",
            "event": {
                "id": 1,
                "name": "Real Madrid vs Barcelona",
                "startTime": "2018-06-20 10:30:00",
                "sport": {
                    "id": 1,
                    "name": "Football"
                },
                "markets": [
                    {
                        "id": 1,
                        "name": "Winner",
                        "selections": [
                            {
                                "id": 1,
                                "name": "Real Madrid",
                                "odds": 1.01
                            },
                            {
                                "id": 2,
                                "name": "Barcelona",
                                "odds": 1.01
                            }
                        ]
                    }
                ]
            }
        }
        self.client.post(self.url, new_event, format='json')
        update_odds = {
            "id": 1,
            "message_type": "UpdateOdds",
            "event": {
                "id": 1,
                "name": "Real Madrid vs Barcelona",
                "startTime": "2018-06-20 10:30:00",
                "sport": {
                    "id": 1,
                    "name": "Football"
                },
                "markets": [
                    {
                        "id": 1,
                        "name": "Winner",
                        "selections": [
                            {
                                "id": 1,
                                "name": "Real Madrid",
                                "odds": 0.5
                            },
                            {
                                "id": 2,
                                "name": "Barcelona",
                                "odds": 1.5
                            }
                        ]
                    }
                ]
            }
        }
        self.client.post(self.url, update_odds, format='json')
        self.assertEqual([0.5, 1.5], [s.odds for s in Selection.objects.all()])
        self.assertEqual(Match.objects.get().name, 'Real Madrid vs Barcelona')
