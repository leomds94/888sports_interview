from rest_framework import serializers

from .models import Message, Match, Sport, Selection, Market


class SelectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Selection
        fields = ['id', 'name', 'odds']


class SportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sport
        fields = ['id', 'name']


class MarketSerializer(serializers.ModelSerializer):
    selections = SelectionSerializer(many=True)

    class Meta:
        model = Market
        fields = ['id', 'name', 'selections']


class MatchSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)
    sport = SportSerializer()
    markets = MarketSerializer(many=True)

    def get_url(self, obj):
        request = self.context.get('request')
        return obj.get_api_url(request=request)

    class Meta:
        model = Match
        fields = ['id', 'url', 'name', 'startTime', 'sport', 'markets']


class MessageSerializer(serializers.ModelSerializer):
    event = MatchSerializer()

    def create(self, validated_data):
        # Separate related objects
        match = validated_data.pop('event')

        market = match.pop('markets', [])[0]
        selections = market.pop('selections', [])
        sport = match.pop('sport')

        sport, sport_created = Sport.objects.get_or_create(
            **sport,
            defaults={'name': sport['name']}
        )
        market, market_created = Market.objects.get_or_create(**market, sport=sport)
        match, match_created = Match.objects.get_or_create(
            **match,
            sport=sport,
            defaults={'name': match['name'], 'startTime': match['startTime']}
        )
        # If match does not exist, create it and related objects (to avoid duplicates)
        if match_created:
            match.markets.add(market)
            for selection_req in selections:
                Selection.objects.create(market=market, **selection_req)
            message = Message.objects.create(event=match, **validated_data)
            return message
        else:
            raise serializers.ValidationError("Event already created")

    def get_url(self, obj):
        request = self.context.get('request')
        return obj.get_api_url(request=request)

    class Meta:
        model = Message
        fields = ['id', 'message_type', 'event']
