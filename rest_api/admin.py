from django.contrib import admin

from .models import Match, Sport, Selection, Market, Message

admin.site.register(Sport)
admin.site.register(Message)
admin.site.register(Market)
admin.site.register(Selection)
admin.site.register(Match)
