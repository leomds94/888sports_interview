"""backend_888sports URL Configuration
"""

from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

schema_view = get_schema_view(
    openapi.Info(
        title="888sports API",
        default_version='v1',
        description="API used for technical interview to 888sports.",
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/', include(('rest_api.urls', 'rest_api'), namespace='api')),
    path('admin/', admin.site.urls),
]
